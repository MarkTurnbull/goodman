/*================================================================================
	goodman.control.checkbox
================================================================================*/

(function($){

	window.goodman.control.range = $.extend(true, window.goodman.control.range || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			// Initilise size bar container
			$('.property_filter_size_bar_container').each(function(){
				var step_point = goodman.config.sizeSteps,
					column_count = step_point.length - 1,
					column_container = $(this).find('.property_filter_size_bar_column_container');
				if ($(this).data('step_point') === undefined) {
					column_container.removeClass('property_filter_size_bar_column_container_5_column');
					column_container.html('');
					for (var i=0;i<column_count;i++) {
						$('<div />',{
							'class':'property_filter_size_bar_column'
						}).appendTo(column_container);
					}
					column_container.addClass('property_filter_size_bar_column_container_'+column_count+'_column');
					$(this).data('step_point',step_point);
				}
			});		
			
			this.update();
			this.enable();
		},

		enable: function() {
			
			this.$body
				.off('.range')
				.on('mousedown.range touchstart.range', '.property_filter_size_bar_container', function(event){
					if ($(event.target).hasClass('property_filter_size_bar_front_start')) {
						$(this).data('mouse_action','set_start');
					}
					if ($(event.target).hasClass('property_filter_size_bar_front_end')) {
						$(this).data('mouse_action','set_end');
					}
				})
				.on('mouseup.range mouseleave.range touchend.range touchcancel.range', '.property_filter_size_bar_container', function(event){
					if ($(this).data('mouse_action')) {
						$(this).data('mouse_action','');
					}
				})
				.on('mousemove.range touchmove.range', '.property_filter_size_bar_container', function(event){
					if (event.touches) { event.pageX = event.touches[0].pageX; }
					if ($(this).data('mouse_action')) {
						var bar_width = $(this).width(),
							bar_left = $(this).offset().left,
							mouse_position = (event.pageX - bar_left) / bar_width,
							bar_step = 1/$(this).find('.property_filter_size_bar_column_container .property_filter_size_bar_column').length,
							bar_start = parseFloat($(this).find('.property_filter_size_bar_front').css('left'))/bar_width,
							bar_end = parseFloat($(this).find('.property_filter_size_bar_front').css('right'))/bar_width;

						if ($(this).data('mouse_action') === 'set_start') {
							var new_bar_start_point = Math.min(Math.round(mouse_position / bar_step), Math.round((1 - bar_end) / bar_step) - 1),
								new_bar_start = new_bar_start_point * bar_step,
								new_bar_start_value = $(this).data('step_point')[new_bar_start_point];
							$(this).parent().find('.property_filter_size_display_start').html( goodman.util.sizeFormat(new_bar_start_value) );
							$(this).parent().find('[name="min"]').val(new_bar_start_value);
							$(this).find('.property_filter_size_bar_front').css('left',(new_bar_start*100)+'%');
							
						}
						if ($(this).data('mouse_action') === 'set_end') {
							var new_bar_end_point = Math.max(Math.round(mouse_position / bar_step), Math.round(bar_start / bar_step) + 1),
								new_bar_end = 1 - new_bar_end_point * bar_step,
								new_bar_end_value = $(this).data('step_point')[new_bar_end_point];
							$(this).parent().find('.property_filter_size_display_end').html( goodman.util.sizeFormat(new_bar_end_value) );
							$(this).parent().find('[name="max"]').val(new_bar_end_value);
							$(this).find('.property_filter_size_bar_front').css('right',(new_bar_end*100)+'%');
						}
					}
				});
			
		},
		
		disable: function() {
			this.$body.off('.range');
		},
		
		update: function() {
			$('.property_filter_size_bar_container').each(function(){
				var $this = $(this),
					$container = $this.closest('.property_filter_content'),
					step_point = $this.data('step_point'),
					bar_step = 1/(step_point.length - 1),
					min = parseInt($('[name="min"]', $container).val()),
					max = parseInt($('[name="max"]', $container).val()),
					minIdx = $.inArray(min, step_point),
					maxIdx = $.inArray(max, step_point);

				if (minIdx === -1) { minIdx = 0; }
				if (maxIdx === -1) { maxIdx = step_point.length - 1; }
				
				var new_bar_start = minIdx * bar_step,
					new_bar_end = 1 - (maxIdx * bar_step);
				
				$container.find('.property_filter_size_display_start').html( goodman.util.sizeFormat(step_point[minIdx]) );
				$this.find('.property_filter_size_bar_front').css('left',(new_bar_start*100)+'%');
				
				$container.find('.property_filter_size_display_end').html( goodman.util.sizeFormat(step_point[maxIdx]) );
				$this.find('.property_filter_size_bar_front').css('right',(new_bar_end*100)+'%');
			});
		}
		
	});

})(jQuery);



// Define the goodman namespace.
var goodman = goodman || {};

(function($){

	window.goodman.interface = $.extend(true, window.goodman.interface || {}, {});
	window.goodman.control = $.extend(true, window.goodman.control || {}, {});

})(jQuery);
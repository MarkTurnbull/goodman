/*================================================================================
	goodman.control.checkbox
================================================================================*/

(function($){

	window.goodman.control.checkbox = $.extend(true, window.goodman.control.checkbox || {}, {

		init: function() {
			this.$body = $('#goodman-smartspace-app');
			this.update();
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.checkbox')
				.on('click.checkbox','.general_style_checkbox_display',function(){
					var checkbox_container = $(this).closest('.general_style_checkbox_container'),
						checkbox_input = checkbox_container.find('.general_style_checkbox_input');
					checkbox_input.click();
				})
				.on('click.checkbox','.general_style_checkbox_input',function(){
					var checkbox_input = $(this),
						checkbox_container = checkbox_input.closest('.general_style_checkbox_container');
					checkbox_container.toggleClass('general_style_checkbox_container_checked', checkbox_input.prop('checked'));
				})
				.on('click.checkbox','.general_style_checkbox_container .general_style_label',function(){
					if (!$(this).attr('for')) {
						var checkbox_container = $(this).closest('.general_style_checkbox_container'),
							checkbox_input = checkbox_container.find('.general_style_checkbox_input');
						checkbox_input.click();
					}
				})
			
				.on('click.checkbox','.general_style_radio_display',function(){
					var radio_container = $(this).closest('.general_style_radio_container'),
						radio_input = radio_container.find('.general_style_radio_input');
					radio_input.click();
				})
				.on('click.checkbox','.general_style_radio_input',function(){
					setTimeout( self.updateRadio, 10 ); // Timeout to let click register
				})
				.on('click.checkbox','.general_style_radio_container .general_style_label',function(){
					if (!$(this).attr('for')) {
						var radio_container = $(this).closest('.general_style_radio_container'),
							radio_input = radio_container.find('.general_style_radio_input');
						radio_input.click();
					}
				});
		},
		
		disable: function() {
			this.$body.off('click.checkbox');
		},
		
		update: function() {
			$('.general_style_checkbox_input').each(function(){
				var checkbox_input = $(this),
					checkbox_container = $(this).closest('.general_style_checkbox_container');
				checkbox_container.toggleClass('general_style_checkbox_container_checked', checkbox_input.prop('checked'));
			});
			this.updateRadio();
		},
		
		updateRadio: function() {
			$('.general_style_radio_input').each(function(){
				var radio_input = $(this),
					radio_container = $(this).closest('.general_style_radio_container');
				radio_container.toggleClass('general_style_radio_container_checked', radio_input.prop('checked'));
			});	
		},
		
	});

})(jQuery);



/*================================================================================
	goodman.control.overlaypopup
================================================================================*/

(function($){

	window.goodman.control.overlaypopup = $.extend(true, window.goodman.control.overlaypopup || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.overlaypopup')
				.on('click.overlaypopup', '.overlay_popup_container', function(e){
					e.stopPropagation();
				})
				.on('click.overlaypopup', '.overlay_popup_close, .overlay_popup_wrapper', function(e){
					self.$body.removeClass('gallery_popup_active resource_popup_active');
				})
				.on('click.overlaypopup', '.gallery_zoom', function(e){
					e.preventDefault();
					self.$body
						.removeClass('gallery_popup_active resource_popup_active')
						.addClass('gallery_popup_active');	
				});
		},
		
		disable: function() {
			this.$body.off('click.overlaypopup');
		},
		
	});

})(jQuery);



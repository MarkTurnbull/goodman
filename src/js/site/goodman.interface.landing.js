/*================================================================================
	goodman.interface.landing
================================================================================*/

(function($){

	window.goodman.interface.landing = $.extend(true, window.goodman.interface.landing || {}, {

		init: function() {
			var self = this;
			this.container = $('#landing-container');
			this.autocompleteContainer = null;
			this.autocompleteContainerIID = -1;
			
			if (!goodman.config.showLanding) { 
				this.container.hide();
				this.container.remove();
				window.parent.postMessage('expand-iframe', '*');
				return false; 
			} 
			
			goodman.observer.subscribe('map-start', self.mapStart, self);
			goodman.observer.subscribe('search-suggest-results', self.suggestionResult, self);
			
			this.container.show();
			this.render();
			this.enable();
		},

		render: function() {
			this.container.html( goodman.template['landing'].hbs({}) );
			this.autocompleteContainer = $('.property_list_search_autocomplete_container', this.container);
			$('.property_list_search_container', this.container).hide();
		},
		
		mapStart: function() {
			$('.loading', this.container).fadeOut(300);
			$('.property_list_search_container', this.container).fadeIn(300);
		},
		
		enable: function() {
			var self = this;
			
			/* Suggestions */
			this.container
				.on('focus.landing keyup.landing', '#property_list_search', function(e){
					e.preventDefault();
					var value = $.trim($(this).val());
					clearTimeout( self.autocompleteContainerIID );
					if (e.which === 38 || e.which === 40 || e.which === 13) { return; }
					if (value !== '') {
						self.autocompleteContainerIID = setTimeout( function(){ goodman.observer.publish('search-suggest', value); }, 250 );
					} else {
						self.previousSearchShow();
					}
				})
				.on('blur.landing', '#property_list_search', function(){
					clearTimeout( self.autocompleteContainerIID );
					self.autocompleteContainerIID = setTimeout( self.suggestionClose, 250 );
				})
				.on('keydown.landing', '#property_list_search', function(e){
					var acWrap = $('.property_list_search_autocomplete_wrapper', self.container),
						suggestions = $('.property_list_search_autocomplete_row', acWrap),
						index = suggestions.filter('.active').index(),
						inputText = $.trim($(this).val());
					switch (e.which) {
						case 38:
							index--;
							if (index === -1) { index = 0; }
							if (index === -2) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 40:
							index++;
							if (index >= suggestions.length) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 13:
							if (index === -1) {
								var text = $.trim($(this).val());
								goodman.observer.publish('suggest-select', inputText );
								goodman.observer.publish('set-promoted', -1, inputText );
								self.copyToSearchAndClose(inputText);
							} else {
								suggestions.filter('.active').trigger('click');
							}
							e.preventDefault();
							
							break;
					}
				
				})
				.on('mouseover.landing', '.property_list_search_autocomplete_row', function(){
					$('.property_list_search_autocomplete_row').removeClass('active');
				})
				.on('click.landing', '[data-suggestion-result]', function(){
					self.suggestionSelect( $(this).data('suggestion-result'), $(this).text() );
				})
				.on('click.landing', '[data-suggestion-saved]', function(){
					self.previousSearchSelect( $(this).data('suggestion-saved'), $(this).text() );
				});
			
		},
		
		disable: function() {
			this.container.off('.landing');
		},
		
		close: function() {
			var self = this;
			window.parent.postMessage('expand-iframe', '*');
			this.container.fadeOut(300, function(){
				self.disable();
				self.container.remove();
				if (goodman.config.isIOS) {
					goodman.interface.propertysearch.renderResults();	
				}
			});		
		},
		
		
		/* ====================================================
		 *	Suggestions
		 * ==================================================*/
		
		suggestionResult: function( suggestions ) {
			var self = this;
			this.suggestionResultData = suggestions;
			
			if ( this.suggestionResultData.numResults > 0 ) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-results'].hbs( this.suggestionResultData ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			} else {
				this.autocompleteContainer
					.html( '' )
					.closest('.property_list_search_autocomplete_wrapper')
					.removeClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		suggestionClose: function() {
			$('.property_list_search_autocomplete_wrapper_expand', this.container).removeClass('property_list_search_autocomplete_wrapper_expand');	
		},
		
		suggestionSelect: function( idx, text ) {
			var self = this,
				s = idx.split(','),
				result = this.suggestionResultData.results[s[0]][s[1]];
			
			$('#property_list_search').val( text );
			if (result.feature && result.feature.attributes.objectid) { 
				goodman.observer.publish('set-promoted', result.feature.attributes.objectid, '' );
			} else {
				goodman.observer.publish('set-promoted', -1, text );
			}
			goodman.interface.propertysearch.previousSearchSave( result );
			goodman.observer.publish('suggest-select', result);
			// this.suggestionClose();
			
			this.copyToSearchAndClose(text);
		},
		
		
		/* ====================================================
		 *	Recent Searches
		 * ==================================================*/
		
		previousSearchShow: function() {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie);
			if (cookie !== undefined && cookie !== '' && cookie.length > 0) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-saved'].hbs( cookie ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		previousSearchClear: function() {
			Cookies.set(goodman.config.previousSearchCookie, '');
		},
		
		previousSearchSelect: function ( idx, text ) {
			var self = this;
			goodman.interface.propertysearch.previousSearchSelect( idx, text );
			// Copy search to search panel
			$('#property-list #property_list_search').val( text ).focus();
			setTimeout ( function() {
				goodman.interface.propertysearch.suggestionClose();
				self.close();
			}, 100);
			
		},
		
		
		/* ====================================================
		 *	Copy to Search and Close
		 * ==================================================*/
		
		copyToSearchAndClose: function ( text ) {
			var self = this,
				searchInput = $('#property-list #property_list_search');
			searchInput.val( text );
			setTimeout ( function() {
				goodman.interface.propertysearch.suggestionClose();
				self.close();
				if (!goodman.config.isMobile) { searchInput.focus(); }
			}, 100);
			
		},
		
		
	});

})(jQuery);



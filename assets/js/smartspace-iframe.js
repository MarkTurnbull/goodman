// Esri iframe listener

function esri_message_listener (e) {
	var root = document.documentElement;
	root.className += ' esri-iframe-expanded';
};


if (window.addEventListener) {
	window.addEventListener('message', esri_message_listener, false);
} else if (window.attachEvent) {
	window.attachEvent('on' + 'message', esri_message_listener);
}

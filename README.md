# Goodman

## Overview

Goodman Global Property Portfolio is a map-based, searchable portfolio of properties.

There are two views of the portfolio: a client facing version, and an internal version with more information.

The portfolio is designed to be rendered in an iframe in a larger site. There is code included for the iframe to expand. The repo includes a dummy iframe holding page to model this. 

## Local Development Setup
  * npm install
  * grunt

## Deploy

Deployment is handled by ESRI

## In Use

https://www.goodman.com/property/global-property-portfolio


## Components

* [gulp](https://gulpjs.com/) - SASS and JS compiling (Note: this is version 3.9.1 of Gulp. May not be compatable with Gulp 4+)
* [jquery](https://api.jquery.com/)
* SASS - Files loosely follow [SASS Guidelines](https://sass-guidelin.es/) 
* [handlebars](https://handlebarsjs.com/) - JS templating
* [Custom Scrollbar](http://manos.malihu.gr/jquery-custom-content-scroller/) - Pretty scrollbars for desktop

## Structure

```
- index.html           Dummy iframe page
- assets               Assets for dummy iframe page
    - css
    - font
    - images
    - js
- smartspace           Main application
    - index.html       Main application HTML
    - assets           Main application assets
        - css
        - font
        - images
        - js
- src                   Source files compiled by Gulp
    - handlebars
    - js
        - iframe        JS for iframe
        - map           JS for map (by ESRI)
        - site          JS for interface
        - vendors
    - scss              Main application CSS
    - scss-iframe       iframe CSS

```


## Functionality

The main functionality is in the scripts in `/src/js/site/` which create the *goodman* javascript namespace. Gulp complies this to `/smartspace/js/goodman.site.min.js`. The order in which these JS files are concatenated is important. 

Gulp also pre-compiles the handlebars templates into `goodman.templates.js`.

```
/src/js/site/goodman.js             - Creates the apt namespace
/src/js/site/goodman.util.js        - Utility functions - number and date formating, etc
/src/js/site/goodman.config.js      - Configuration settings for the application (will be extended by `smartspaceConfig` in index.html)
/src/js/site/goodman.observer.js    - Handles publish / subscribe 
/src/js/site/goodman.handlebars.js  - Application specific Handlebars extensions and utility functions 
/src/js/site/goodman.data.js        - Reading, preprocessing and managing data
/src/js/site/goodman.control.*.js   - Sub elements of the frontend - custom checkboxes, dropdown lists, image galleries, etc
/src/js/site/goodman.interface.js   - Main frontend interfaces namespace
/src/js/site/goodman.interface.*.js - Main frontend interfaces
/src/js/site/goodman.init.js        - Initialises the application
```

The application is split into interfaces: landing page, map, property search, and property detail. The code for these are in the relevant `/src/js/site/goodman.interface.*.js` file

## Data management

Data comes from the map via events. Events are listed at the bottom of this page.


## Settings

These values can be set in the index.html `smartspaceConfig` javascript variable:
```
showLanding  - Boolean (default: true) - If true show the landing page property search input, otherwise go directly to the map interface.
isInternal   - Boolean (default: false) - If true present the Goodman Internal view of the portfolio, otherwise show the external view.
```

## Quirks

**HANDLEBARS**

Handlebars provides HTML templates for javascript. Templates are typically invoked using this pattern:
```JS
$('.DOM-object-to-inject-content-into').html( goodman.template['HB-template-name-from-filename'].hbs({ 'example':'Content to be used in template' }) );
```

**PUBLISH/SUBSCRIBE PATTERN**

Events are passed around the application using a publish / subscribe pattern. Code is found in /src/js/site/goodman.observer.js

To subscribe to an event:
```JS
goodman.observer.subscribe('example-event', callbackFunction, [context]);
```
Context is optional but generally is required or 'this' stops pointing at the thing you think it should.

To publish (broadcast) an event:
```JS
goodman.observer.publish('example-event', data);
```


# Events

These are the events that allow the interface (Source: JSA) to talk to the map (Source: ESRI)

### search

  * Source: JSA
  * Requirement: N/A
  * Purpose: Search  
  * Parameters: `{ type: 'all|industrial|commercial|land', term: 'string-to-search' }`
  
### search-busy

  * Source: ESRI
  * Requirement: N/A
  * Purpose: Indicate when search is processing 
  * Parameters: `true|false`
  
### search-result

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return the results of a search
  * Parameters: result object  
  
## Search suggestion events

### search-suggest

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request search suggestions
  * Parameters: search term 
  
### search-suggest-results

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return suggestions for a search
  * Parameters: suggestion list object  

### suggest-select

  * Source: JSA
  * Requirement: N/A
  * Purpose: Select a search suggestions
  * Parameters: Suggestion object
  
  
## Property detail events

### detail-request

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request details of single object
  * Parameters: objectID
  
### detail-open

  * Source: Esri
  * Requirement: N/A
  * Purpose: Open detail display panel with supplied data
  * Parameters: Property details object

### get-related
	
  * Source: JSA
  * Requirement: N/A
  * Purpose: Request related resources of single object
  * Parameters: objectID, 'resource'|'sitefeature'|'site'|'locality'
	
### return-related

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return related resources of single object
  * Parameters: Related resources object
  
### get-children
	
  * Source: JSA
  * Requirement: N/A
  * Purpose: Request children/siblings of an object, but not the object itself
  * Parameters: objectID, parentToHaveID (object ID for the parent)

### return-child-properties
	
  * Source: Esri
  * Requirement: N/A
  * Purpose: Return children/siblings of single object
  * Parameters: [featureset of properties]

### select-locality

  * Source: JSA
  * Requirement: N/A
  * Purpose: Select related locality
  * Parameters: locality objectID, property objectID

## Map type

### set-map-type

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request map change type
  * Parameters: "streets"|"satellite"

### map-type

  * Source: Esri
  * Requirement: N/A
  * Purpose: Tell UI the amp type
  * Parameters: "streets"|"satellite"


## Geolocation  
  
### get-location

  * Source: Esri
  * Requirement: N/A
  * Purpose: Request browser return geolocation
  * Parameters: none
  
### set-location

  * Source: JSA
  * Requirement: N/A
  * Purpose: Return browser geolocation
  * Parameters: JS position object 